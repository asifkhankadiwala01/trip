<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class VehicleController extends Controller
{

    public function index()
    {
        return view('vehicles.index');
    }

    
    public function list(DataTables $datatables,Request $request){
        // $user = auth()->user();
        $query = \DB::table('vehicles');
        /*if(!empty($user->region) && strtolower($user->region) != 'all') {
            $query->whereRaw('(region) = ?',["{$user->region}"]);
        }
        if(!empty($user->territory) && strtolower($user->territory) != 'all') {
            $query->whereRaw('(territory) = ?',["{$user->territory}"]);
        }*/
        // return $datatables->eloquent(HCP::query())
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'vehicles']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        $drivers = User::where('urole','driver')->select('id','identityNumber')->get();
        $companies = Companies::get();
        $activities = config('constants.activities');
        return view('vehicles.create', compact('companies','activities','drivers'));
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
        try {
          
           Vehicle::create($data);

            return redirect()->route('vehicles.index')
                             ->with('success_message', 'Labor Item was successfully added.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $vehicle = Vehicle::findOrFail($id);

        return view('vehicles.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {   
        $vehicle =  Vehicle::findOrFail($id);
        $drivers = User::where('urole','driver')->select('id','identityNumber')->get();
        $companies = Companies::get();
        $activities = config('constants.activities');
        return view('vehicles.edit', compact('vehicle','drivers','companies','activities'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
            $vehicle =  Vehicle::findOrFail($id);
            $vehicle->update($data);

            return redirect()->route('vehicles.index')
                             ->with('success_message', 'Labor Item was successfully updated.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $vehicle = Vehicle::findOrFail($id);
            $vehicle->delete();

            return redirect()->route('vehicles.index')
                             ->with('success_message', 'Labor Item was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'waslDriverId' => 'required',
            'waslCompanyId' => 'required',
            'gpsGateUserId' => 'required',
            'gpsGateServerId' => 'required',
            'sequenceNumber' => 'required',
            'plateType' => 'required',
            'plateNumber' => 'required',
            'plateRightLetter' => 'required',
            'plateMiddleLetter' => 'required',
            'plateLeftLetter' => 'required',
            'imeiNumber' => 'required',
            'waslActivity' => 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
