<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Screen;
use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class ScreenController extends Controller
{

    public function index() {
        return view('screens.index');
    }

    
    public function list(DataTables $datatables,Request $request) 
    {
        $query = \DB::table('screens');
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'screens']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        $companies = Companies::get();
        return view('screens.create', compact('companies'));
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
        $screen_data = ["screen_id"=> $data['screen_id'],"company_id" => $data['company_id'],"rest_user_id" => $data['rest_user_id']];
        
        Screen::create($data);
        return redirect()->route('screens.index')->with('success_message', 'Screen was successfully added.');
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $screens = Screen::findOrFail($id);
        return view('screens.show', compact('screens'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {   
        $companies =  Companies::get();
        $screens =  Screen::findOrFail($id);
        return view('screens.edit', compact('screens','companies'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
            $vehicle =  Screen::findOrFail($id);
            $vehicle->update($data);

            return redirect()->route('screens.index')
                             ->with('success_message', 'Screen was successfully updated.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $vehicle = Screen::findOrFail($id);
            $vehicle->delete();

            return redirect()->route('screens.index')
                             ->with('success_message', 'Screen was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            "screen_id"=> 'required',
            "company_id"=> 'required',
            "rest_user_id"=> 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
