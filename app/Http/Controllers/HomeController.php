<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Station;
use App\Models\Vehicle;
use App\Models\Trip;
use App\Models\User;
use App\Models\Companies;
use App\Models\Screen;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $station = Station::count();
        $vehicle = Vehicle::count();
        $trip = Trip::count();
        $user = User::count();
        $companies = Companies::count();
        $screens = Screen::count();
        return view('home',compact('station','vehicle','trip','user','companies','screens'));
    }
}
