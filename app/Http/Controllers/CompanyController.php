<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class CompanyController extends Controller
{

    public function index() {
        return view('companies.index');
    }

    
    public function list(DataTables $datatables,Request $request) {

        $query = \DB::table('companies');
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'companies']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        return view('companies.create');
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
        $company_data = ["companyName"=> $data['companyName'],"companyId" => $data['companyId']];
        
        Companies::create($data);

        return redirect()->route('companies.index')->with('success_message', 'Company was successfully added.');
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $companies = Companies::findOrFail($id);
        return view('companies.show', compact('companies'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {   
        $companies =  Companies::findOrFail($id);
        return view('companies.edit', compact('companies'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
            $vehicle =  Companies::findOrFail($id);
            $vehicle->update($data);

            return redirect()->route('companies.index')
                             ->with('success_message', 'Companies was successfully updated.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $vehicle = Companies::findOrFail($id);
            $vehicle->delete();

            return redirect()->route('companies.index')
                             ->with('success_message', 'Companies was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            "companyName"=> 'required',
            "companyId"=> 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
