<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

class ImportController extends Controller
{
    public function uploadContent(Request $request)
    {   


    	  	$filepath = public_path("city.csv");
            dd($filepath);
            // Reading file
            $file = fopen($filepath, "r");
            $importData_arr = array(); // Read through the file and store the contents as an array
            $i = 0;
            //Read the contents of the uploaded file
            while (($filedata = fgetcsv($file, 1000, ",")) !== false)
            {
                $num = count($filedata);

                // Skip first row (Remove below comment if you want to skip the first row)
                if ($i == 0)
                {
                    $i++;
                    continue;
                }
                for ($c = 0;$c < $num;$c++)
                { 
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file); //Close after reading
            $j = 0;
            foreach ($importData_arr as $data)
            { 
                $j++;
                try
                {
					
					$arr=[
						'code' => $data[0],
						'arabicName' => $data[1],
						'englishName' => $data[2],					
					];
					// dd($arr);
                    DB::beginTransaction();
                    \App\Models\City::create($arr);
               
                    DB::commit();
                }
                catch(\Exception $e)
                {

                    dd($e);                    //throw $th;
                    DB::rollBack();
                }
            }


    }
     
}

