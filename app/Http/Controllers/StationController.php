<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Station;
use App\Models\City;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class StationController extends Controller
{

    public function index()
    {
        return view('stations.index');
    }

    
    public function list(DataTables $datatables,Request $request){
        // $user = auth()->user();
        $query = \DB::table('stations');
        /*if(!empty($user->region) && strtolower($user->region) != 'all') {
            $query->whereRaw('(region) = ?',["{$user->region}"]);
        }
        if(!empty($user->territory) && strtolower($user->territory) != 'all') {
            $query->whereRaw('(territory) = ?',["{$user->territory}"]);
        }*/
        // return $datatables->eloquent(HCP::query())
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'stations']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        $cities = City::select('englishName','code')->get();
        $companies = [(object)['id'=> "f91ceb6c-c59f-4b26-ab31-d9654d1cdbe5",'name'=> 'f91ceb6c-c59f-4b26-ab31-d9654d1cdbe5']];
        $activities = config('constants.activities');
        return view('stations.create', compact('companies','activities','cities'));
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
        try {
            
			$station_data = ["stationCode"=> $data['stationCode'],"arabicName" => $data['arabicName'],"englishName"=> $data['englishName'],"latitude"=> (float)$data['latitude'],"longitude"=> (float)$data['longitude'],"cityCode"=> $data['cityCode'],"stationStatus"=> $data['stationStatus'],"email"=> $data['email'],"phone"=> $data['phone'],"phoneExtension"=> $data['phoneExtension'],"managerPhone"=> $data['managerPhone']];
			$json_data = json_encode($station_data);
			
			echo $json_data;
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://wasl.tga.gov.sa/api/tracking/v1/station',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $json_data,
			  CURLOPT_HTTPHEADER => array(
				'x-api-key: 1190c6bb-ea86-4302-bcbb-e426a279ce0b',
				'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$result = json_decode($response);
			
			if($result->success){
				Station::create($data);
				return redirect()->route('stations.index')
                             ->with('success_message', 'Station was successfully added.');
			} else {
				return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
			}
        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $station = Station::findOrFail($id);

        return view('stations.show', compact('station'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $station =  Station::findOrFail($id);
        $cities = City::select('englishName','code')->get();

        $companies = [(object)['id'=> "f91ceb6c-c59f-4b26-ab31-d9654d1cdbe5",'name'=> 'f91ceb6c-c59f-4b26-ab31-d9654d1cdbe5']];
        $activities = config('constants.activities');
        return view('stations.edit', compact('station','companies','activities','cities'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
            $statation =  Station::findOrFail($id);
			$station_data = ["stationCode"=> $data['stationCode'],"arabicName" => $data['arabicName'],"englishName"=> $data['englishName'],"latitude"=> (float)$data['latitude'],"longitude"=> (float)$data['longitude'],"cityCode"=> $data['cityCode'],"stationStatus"=> $data['stationStatus'],"email"=> $data['email'],"phone"=> $data['phone'],"phoneExtension"=> $data['phoneExtension'],"managerPhone"=> $data['managerPhone']];
			$json_data = json_encode($station_data);
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://wasl.tga.gov.sa/api/tracking/v1/station',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $json_data,
			  CURLOPT_HTTPHEADER => array(
				'x-api-key: 1190c6bb-ea86-4302-bcbb-e426a279ce0b',
				'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$result = json_decode($response);
			if($result->success){
				
				$statation->update($data);

				return redirect()->route('stations.index')
                             ->with('success_message', 'Station was successfully updated.');
			} else{
				return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
			}

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $driver = Station::findOrFail($id);
            $driver->delete();

            return redirect()->route('stations.index')
                             ->with('success_message', 'Labor Item was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'stationCode' => 'required',
            'arabicName' => 'required',
            'englishName' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'cityCode' => 'required',
            'stationStatus' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'phoneExtension' => 'required',
            'managerPhone' => 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
