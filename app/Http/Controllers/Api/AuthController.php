<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Validator;

class AuthController extends Controller
{ 
	/**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
        $this->guard = "api";
    }
	
	/**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'identity_number' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            $message = $validator->errors()->first();
            $success = [
                'message' => $message,
                'success' => false
            ];
            return $this->sendResponse($success);
        }

        $user = User::select(
            'id',
            'identityNumber',
            'dateOfBirthHijri',
            'mobileNumber',
            'waslCompanyId',
            'waslActivity',
            'urole'
        )->where('identityNumber', request('identity_number'))->first();
		
		if (!$token = $this->attemptLogin($user)) {
            $success = [
                'message' => "Invalid email or password.",
                'success' => false
            ];
            return $this->sendResponse($success);
        }
		
        if (!$user) {
            $success = [
                'message' => "Invalid driver identiry number or password.",
                'success' => false
            ];
            return $this->sendResponse($success);
        } 
        if (!$token =\JWTAuth::fromUser($user)) {
            $success = [
                'message' => "Invalid driver identiry number or password.",
                'success' => false
            ];
            return $this->sendResponse($success);
        } 
        
        $user->token = $token;

        $success = [
                'success' => true,
                'message' => 'Login Successfully',
                'data' => $user
            ];
        return $this->sendResponse($success);
    }
	
	public function attemptLogin(User $user)
    {
        if (!$token = auth($this->guard)->attempt(['identityNumber' => request('identity_number'), 'password' => request('password')])) {
            $token = auth($this->guard)->attempt(['identityNumber' => request('identity_number'), 'password' => request('password')]);
        }   
        return $token;
    }
}
