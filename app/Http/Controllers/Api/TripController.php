<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Trip;
use App\Models\TripUpdate;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;

class TripController extends Controller
{ 
    public function getTrips(Request $request)
    {
        $user = auth()->user();
        $identityNumber = $user->identityNumber;
 
        $trips = Trip::with('tripupdates')->whereRaw('(primaryDriver = "'.$identityNumber.'" or assistantDriver = "'.$identityNumber.'"  )')->where('trip_status','!=','ENDED')->get();

        $success = [
                'success' => true,
                'message' => 'Trips get succcessfully',
                'data' => $trips
        ];
        return $this->sendResponse($success);
    }


    public function ChangeTripsStatus(Request $request)
    {
        $user = auth()->user();
        $identityNumber = $user->identityNumber;
        
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required', 
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
            foreach($errors->all() as $key=>$error){
                if($key == 0)
                    $message = $error;
            }
            $success = [
                'message' => $message,
                'success' => false
            ];
            return $this->sendResponse($success);
            //return $this->sendError($validator->errors());       
        }

        $trip = Trip::whereRaw('(primaryDriver = "'.$identityNumber.'" or assistantDriver = "'.$identityNumber.'"  )')->find($request->trip_id);

        if($trip){
			
			$trip_data = ["driverId"=> $identityNumber,"numberOfPassengersIn" => (int)$request->numberOfPassengersIn,"numberOfPassengersOut" => (int)$request->numberOfPassengersOut,"tripStatus" => $request->tripStatus,"currentStationCode" => $request->currentStationCode,"tripUpdateDateTime" => \Carbon\Carbon::now()->format('Y-m-d\TH:i:s.z\Z'),"activity" => $request->activity];
			$json_data = json_encode($trip_data);
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://wasl.tga.gov.sa/api/tracking/v1/operating-companies/'.$trip->companyId.'/bus-trips/'.$trip->tripNumber,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'PATCH',
			  CURLOPT_POSTFIELDS => $json_data,
			  CURLOPT_HTTPHEADER => array(
				'x-api-key: 1190c6bb-ea86-4302-bcbb-e426a279ce0b',
				'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);

			$result = json_decode($response);
			if(isset($result->errorCode) && $result->errorCode == "bad_request"){
				$success = [
                    'success' => false,
                    'message' => $result->errorMsg,
				];
				return $this->sendResponse($success);
			}
			if(isset($result->success) && $result->success == false)
			{	
				$success = [
                    'success' => false,
                    'message' => str_replace("_"," ",$result->resultCode),
				];
				return $this->sendResponse($success);
			}
			if($result->success){
				$tripupdate = new TripUpdate();
				$tripupdate->trip_id = $request->trip_id;
				$tripupdate->driverId = $identityNumber;
				$tripupdate->numberOfPassengersIn = $request->numberOfPassengersIn;
				$tripupdate->numberOfPassengersOut = $request->numberOfPassengersOut;
				$tripupdate->tripStatus = $request->tripStatus;
				$tripupdate->currentStationCode = $request->currentStationCode;
				$tripupdate->tripUpdateDateTime = \Carbon\Carbon::now()->format('Y-m-d\TH:i:s.z\Z');
				$tripupdate->activity = $request->activity;
				$tripupdate->wasl_response = $response;
				$tripupdate->save();
				
				$trip->update(['trip_status'=>$request->tripStatus]);
				$success = [
                    'success' => true,
                    'message' => 'Trip updated succcessfully',
                    'data' => $tripupdate
				];
			} else {
				$success = [
                    'success' => false,
                    'message' => 'wasl response failed'
				];
			}
            return $this->sendResponse($success);
        }

        $success = [
                    'success' => false,
                    'message' => 'Something went wrong'
        ];
        return $this->sendResponse($success);
    }
}
