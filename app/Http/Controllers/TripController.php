<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Trip;
use App\Models\Vehicle;
use App\Models\Station;
use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class TripController extends Controller
{

    public function index() {
        return view('trips.index');
    }

    
    public function list(DataTables $datatables,Request $request) {
        // $user = auth()->user();
        $query = \DB::table('trips');
        /*if(!empty($user->region) && strtolower($user->region) != 'all') {
            $query->whereRaw('(region) = ?',["{$user->region}"]);
        }
        if(!empty($user->territory) && strtolower($user->territory) != 'all') {
            $query->whereRaw('(territory) = ?',["{$user->territory}"]);
        }*/
        // return $datatables->eloquent(HCP::query())
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'trips']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        $drivers = User::where('urole','driver')->select('identityNumber')->get();
        $vehicles = Vehicle::get();
        $stations = Station::get();

        $tripTypes = config('constants.tripTypes');
        $companies = Companies::get();
        $activities = config('constants.activities');
        return view('trips.create', compact('tripTypes','stations','vehicles','companies','activities','drivers'));
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
        try {
			$data['expectedDepartureTime'] = str_replace('+00:00', '.000Z', gmdate('c', (strtotime($data['expectedDepartureTime'])+10800)));
			$data['expectedArrivalTime'] = str_replace('+00:00', '.000Z', gmdate('c', (strtotime($data['expectedArrivalTime'])+10800)));
			
			$trip_data = ["vehicle"=> $data['vehicle'],"primaryDriver" => $data['primaryDriver'],"assistantDriver"=> $data['assistantDriver'],"tripNumber"=> $data['tripNumber'],"expectedDepartureTime"=> $data['expectedDepartureTime'],"expectedArrivalTime"=> $data['expectedArrivalTime'],"distanceInMeters"=> $data['distanceInMeters'],"tripType"=> $data['type'],"activity"=> $data['activity'],"departureStationCode"=> $data['departureStationCode'],"arrivalStationCode"=> $data['arrivalStationCode'],"numberOfPassengers"=> $data['numberOfPassengers']];
			$json_data = json_encode($trip_data);
			
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://wasl.tga.gov.sa/api/tracking/v1/operating-companies/'.$data["companyId"].'/bus-trips',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $json_data,
			  CURLOPT_HTTPHEADER => array(
				'x-api-key: 1190c6bb-ea86-4302-bcbb-e426a279ce0b',
				'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$result = json_decode($response);
			if(isset($result->errorCode) && $result->errorCode == "bad_request"){
				return back()->withInput()->withErrors(['unexpected_error' => $result->errorMsg]);
			}
			if(isset($result->success) && $result->success == false){
				return back()->withInput()
                         ->withErrors(['unexpected_error' => str_replace("_"," ",$result->resultCode)]);
			}
			if($result->success){
				$data['wasl_response'] = $response;
				Trip::create($data);

				return redirect()->route('trips.index')->with('success_message', 'Trip was successfully added.');
			} else {
				return back()->withInput()->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
			}
        } catch (Exception $exception) {
            return back()->withInput()->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $trip = Trip::findOrFail($id);

        return view('trips.show', compact('trip'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {   
        $trip =  Trip::findOrFail($id);
        $vehicles =  Vehicle::get();
        $stations =  Station::get();
        $drivers = User::where('urole','driver')->select('identityNumber')->get();
        $companies = Companies::get();
        $activities = config('constants.activities');
        $tripTypes = config('constants.tripTypes');
        return view('trips.edit', compact('trip','tripTypes','stations','vehicles','drivers','companies','activities'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
            $vehicle =  Trip::findOrFail($id);
            $vehicle->update($data);

            return redirect()->route('trips.index')
                             ->with('success_message', 'Trip was successfully updated.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $vehicle = Trip::findOrFail($id);
            $vehicle->delete();

            return redirect()->route('trips.index')
                             ->with('success_message', 'Trip was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            "vehicle"=> 'required',
            "primaryDriver"=> 'required',
            "assistantDriver"=> 'required',
            "tripNumber"=> 'required',
            "expectedDepartureTime"=> 'required',
            "expectedArrivalTime"=> 'required',
            "distanceInMeters"=> 'required',
            "numberOfPassengers"=> 'required',
            "type"=> 'required',
            "activity"=> 'required',
            "departureStationCode"=> 'required',
            "arrivalStationCode"=> 'required',
            "companyId"=> 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
