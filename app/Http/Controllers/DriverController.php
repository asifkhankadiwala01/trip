<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Exception;

class DriverController extends Controller
{

    public function index()
    { 
        // $driver = User::where('urole','driver')->paginate(25);

        return view('driver.index');
    }

    
    public function list(DataTables $datatables,Request $request){
        // $user = auth()->user();
        $query = \DB::table('users')->where('urole','driver');
        /*if(!empty($user->region) && strtolower($user->region) != 'all') {
            $query->whereRaw('(region) = ?',["{$user->region}"]);
        }
        if(!empty($user->territory) && strtolower($user->territory) != 'all') {
            $query->whereRaw('(territory) = ?',["{$user->territory}"]);
        }*/
        // return $datatables->eloquent(HCP::query())
        return   $datatables->of($query)
            ->addColumn('action',function ($table) {
                return view('tables.action',['table'=>$table,'prefix'=>'driver']);
            })
            ->rawColumns(['action'])
            ->toJson();
    }



    public function create()
    {
        $companies = Companies::get();
        $activities = config('constants.activities');
        return view('driver.create', compact('companies','activities'));
    }

    public function store(Request $request)
    {
        $data = $this->getData($request);
		$data1 = $this->getPasswordData($request);
		
        try {			
			$data['password'] = \Hash::make($request->password);
            User::create($data);

            return redirect()->route('driver.index')
                             ->with('success_message', 'Labor Item was successfully added.');

        } catch (Exception $exception) {
			
            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $driver = User::where('urole','driver')->findOrFail($id);

        return view('driver.show', compact('driver'));
    }

    /**
     * Show the form for editing the specified labor item.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $driver =  User::where('urole','driver')->findOrFail($id);
        $companies = Companies::get();
        $activities = config('constants.activities');
        return view('driver.edit', compact('driver','companies','activities'));
    }

    /**
     * Update the specified labor item in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $data = $this->getData($request);
        try {
			if(!empty($request->password)){
				$data['password'] = \Hash::make($request->password);
			}
            $driver =  User::where('urole','driver')->findOrFail($id);
            $driver->update($data);

            return redirect()->route('driver.index')
                             ->with('success_message', 'Labor Item was successfully updated.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified labor item from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $driver = User::where('urole','driver')->findOrFail($id);
            $driver->delete();

            return redirect()->route('driver.index')
                             ->with('success_message', 'Labor Item was successfully deleted.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'identityNumber' => 'required',
            'dateOfBirthHijri' => 'required',
            'mobileNumber' => 'required',
            'waslCompanyId' => 'required',
            'waslActivity' => 'required',
        ];
        $data = $request->validate($rules);
        
        return $data;
    }
	
	protected function getPasswordData(Request $request)
    {
        $rules = [
            'password' => 'required'
        ];
        $data = $request->validate($rules);
        
        return $data;
    }

}
