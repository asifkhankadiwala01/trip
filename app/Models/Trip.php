<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;
    protected $fillable = [
        "vehicle",
        "primaryDriver",
        "assistantDriver",
        "tripNumber",
        "expectedDepartureTime",
        "expectedArrivalTime",
        "distanceInMeters",
        "numberOfPassengers",
        "type",
        "activity",
        "departureStationCode",
        "arrivalStationCode",
        "companyId",
        "trip_status",
        "wsal_response",
    ];
    
    public function tripupdates()
    {
        return $this->hasMany('App\Models\TripUpdate', 'trip_id', 'id')->latest();
    }
}
