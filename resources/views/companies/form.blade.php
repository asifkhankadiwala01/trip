<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
	@error('unexpected_error')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
	@enderror

	<div class="form-group">
		<label for="name1">Company Name</label>
		<input type="text" id="companyname" name="companyName" class="form-control" placeholder="Enter Company Name" value="{{old('companyName', optional($companies)->companyName)}}" > 
		@error('companyname')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<div class="form-group">
		<label for="name1">Company Id</label>
		<input type="text" id="companyid" name="companyId" class="form-control" placeholder="Enter Company Id" value="{{old('companyId', optional($companies)->companyId)}}" > 
		@error('companyid')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('companies.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>