@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Edit Company</h4>
					</div>
				</div>
				 <div class="action-btn">
                        <a href="{{ route('companies.index') }}" class="btn px-15 btn-primary" >Cancel</a>
                 </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-30 py-sm-50 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-12 col-lg-10">
						<div class="mx-sm-30 mx-20 ">
							<!-- Start: card -->
							<!-- <div class="card add-product p-sm-30 p-20 mb-30">
									<div class="card-body p-0">
										<div class="card-header">
											<h6 class="fw-500">Add Product</h6>
										</div> -->
										<!-- Start: card body -->
										<div class="add-product__body px-sm-40 px-20">
											<form method="POST" action="{{ route('companies.update',$companies->id) }}" accept-charset="UTF-8" id="create_user_form" name="create_user_form" class="form-horizontal" enctype="multipart/form-data">
													{{ csrf_field() }}
													@include ('companies.form', ['companies' => $companies])
											</form>
										</div>
								<!-- </div>
							</div> -->
					
						</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
</div>
@endsection
@push('css')
	<link rel="stylesheet" href="/libraries/assets/vendor_assets/css/line-awesome.min.css">
	 <link rel="stylesheet" href="/css/daterangepicker.css">
	 <style type="text/css">
	 		.daterangepicker  .ranges{
	 			display: none !important;
	 		}
	 		.daterangepicker.single .calendar-table tbody td.today, .daterangepicker.single .calendar-table tbody td.active {
	 			color: #5f63f2 !important;
	 		}
	 		.daterangepicker.show-calendar .calendar-table tbody td.available.active.end-date:before {
	 			display: none !important;
	 		}
	 		.daterangepicker.show-calendar .calendar-table tbody td.available.active.start-date:after, 
	 		.daterangepicker.show-calendar .calendar-table tbody td.available.active.start-date:before,
	 		.daterangepicker.show-calendar .calendar-table tbody td.available.active.end-date:after,
	 		.daterangepicker.show-calendar .calendar-table tbody td.available.active.end-date:before,
	 		.daterangepicker.show-calendar .calendar-table tbody td.today,
	 		.daterangepicker.show-calendar .calendar-table tbody td.today:after {
	 			    border-radius: 6px !important;
	 		} 

	 		.drp-buttons  {
	 			display: flex!important;
	 		}
	 		.drp-buttons .btn-sm {
	 			    line-height: 1.5rem !important;
	 		}
	 		.daterangepicker .calendar-time {
	 			margin-top: 20px;
	 		}
			.daterangepicker select.hourselect, .daterangepicker select.minuteselect, .daterangepicker select.secondselect, .daterangepicker select.ampmselect
	 		{
	 			    font-size: 14px;
				    padding: 4px 10px;
				    width: 70px;
				    background: #fff;
	 		}
	 		.daterangepicker.single .drp-calendar.left {
			    padding: 0 20px 2px;
			}
	 </style>
 @endpush
 @push('scripts')
 		<script src="/js/daterangepicker.js"></script>
<script >
 
  $( document ).ready(function() {
	  $('.start_time').daterangepicker({
    	timePicker: true,
	    singleDatePicker: true,
	    autoUpdateInput: false,
	    timePicker24Hour:true,
	     locale: {
	      format: 'YYYY-MM-DD HH:mm'
	    }
	  }, function(start, end, label) {
	  		$('.start_time').val(start.format('YYYY-MM-DD HH:mm'))
		});


	  $(".end_time").daterangepicker({
    	timePicker: true,
	    singleDatePicker: true,
	    autoUpdateInput: false,
	    timePicker24Hour:true,
	     locale: {
	      format: 'YYYY-MM-DD HH:mm'
	    }
	  }, function(start, end, label) {
		  		$('.end_time').val(start.format('YYYY-MM-DD HH:mm'))
		});
  });
 </script>

 @endpush

