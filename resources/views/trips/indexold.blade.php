@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="note-wrapper">
    <div class="row">
        <div class="col-lg-12">

            <div class="breadcrumb-main">
                <h4 class="text-capitalize breadcrumb-title">Dashboard</h4>
                <div class="breadcrumb-action justify-content-center flex-wrap">
                </div>
            </div>

        </div>
        <div class="col-lg-12">
            <div class="note-contents">
       
                <!-- ends: .col-lg-2 -->
                <div class="note-grid-wrapper mb-30">
                    <div class="notes-wrapper">
                        <div class="note-grid">
                            <div class="note-single">

                                <div class="note-card note-important">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Landing Page Design <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-social">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Socail Human Being <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-personal">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Landing Page Development <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-default">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Shop Page Design <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-work">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Plugin Development <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-default">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Theme Development <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-important">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Improve Writing Skill <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="note-single">

                                <div class="note-card note-social">
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <h4 class="note-title">Keep Social Distance <span class="note-status"></span></h4>
                                            <p class="note-excerpt">Lorem Ipsum is simply dummy text of the printing</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- ends: .col-lg-10 -->
            </div>
        </div><!-- ends: .col-lg-12 -->
    </div>
</div>
    </div>
 
@endsection 