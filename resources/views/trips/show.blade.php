@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Trip Detail</h4>
					</div>
				</div>
				<div class="action-btn">
                	<a href="{{ route('trips.index') }}" class="btn px-15 btn-primary" >Trip List</a>
         		</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-15 py-sm-30 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-6 col-lg-6">
						<div class="mx-sm-30 mx-20 ">
					 
							<div class=" "> 

								<div class="div mb-3">
				                    <h6>Vehicle</h6>
				                    <span>{{ $trip->vehicle }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Primary Driver</h6>
				                    <span>{{ $trip->primaryDriver }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Assistant Driver</h6>
				                    <span>{{ $trip->assistantDriver }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Trip Number</h6>
				                    <span>{{ $trip->tripNumber }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Expected Departure Time</h6>
				                    <span>{{ $trip->expectedDepartureTime }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Expected Arrival Time</h6>
				                    <span>{{ $trip->expectedArrivalTime }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Distance In Meters</h6>
				                    <span>{{ $trip->distanceInMeters }}</span>
				                </div> 
						  
							
						  
							</div>
							 
					
						</div>
					</div>
					<div class="col-xl-6 col-lg-6">
						<div class="mx-sm-30 mx-20 ">
								<div class="div mb-3">
				                    <h6>Number Of Passengers</h6>
				                    <span>{{ $trip->numberOfPassengers }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Type</h6>
				                    <span>{{ $trip->type }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Activity</h6>
				                    <span>{{ $trip->activity }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Departure Station Code</h6>
				                    <span>{{ $trip->departureStationCode }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Arrival Station Code</h6>
				                    <span>{{ $trip->arrivalStationCode }}</span>
				                </div> 
						  
								<div class="div mb-3">
				                    <h6>Company Id</h6>
				                    <span>{{ $trip->companyId }}</span>
				                </div> 
								<div class="div mb-3">
				                    <h6>Wasl Response</h6>
				                    <span>{{ $trip->wasl_response }}</span>
				                </div> 
					</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
 

	<div class="card">
            <div class="card-header color-dark fw-500">
                Trip Updates
            </div>
            <div class="card-body p-0">
                <div class="table4  p-25 bg-white mb-30">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead>
                                <tr class="userDatatable-header">
                                    <th>
                                        <span class="userDatatable-title">Driver Id</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Number Of Passengers In</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Number Of Passengers Out</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Trip Status</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Current Station Code</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Trip Update Date Time</span>
                                    </th>
                                    <th>
                                        <span class="userDatatable-title">Activity</span>
                                    </th> 
									<th>
                                        <span class="userDatatable-title">Wasl Response</span>
                                    </th>
							</tr>
                            </thead>
                            <tbody>
                            	 
                            	@foreach($trip->tripupdates as $tripupdate)
                                <tr>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->driverId}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->numberOfPassengersIn}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->numberOfPassengersOut}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->tripStatus}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->currentStationCode}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->tripUpdateDateTime}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->activity}}
                                        </div>
                                    </td>
									<td>
                                        <div class="userDatatable-content">
                                            {{$tripupdate->wasl_response}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection

 
