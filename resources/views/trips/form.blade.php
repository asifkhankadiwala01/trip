<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
	@error('unexpected_error')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
	@enderror
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Vehicle Id </label> 
			<select class="js-example-basic-single js-states form-control" id="vehicle" name="vehicle" >
				<option value="" style="display:none;">Select Vehicle</option>
                @foreach ($vehicles as $value)
                    <option value="{{ $value->sequenceNumber }}"  {{ ( $value->sequenceNumber == old('vehicle',optional($trip)->vehicle) )? 'selected' : '' }}>{{$value->sequenceNumber}}</option>
                @endforeach
			</select>
			@error('vehicle')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Primary Driver
			</label>
			<select class="js-example-basic-single js-states form-control" id="primaryDriver" name="primaryDriver" >
				<option value="" style="display:none;">Select Primary Driver</option>
                @foreach ($drivers as $value)
                    <option value="{{ $value->identityNumber }}"  {{ ( $value->identityNumber == old('primaryDriver',optional($trip)->primaryDriver) ) ? 'selected' : '' }}>{{$value->identityNumber}}</option>
                @endforeach
			</select>
			@error('primaryDriver')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Assistant Driver
			</label>
			<select class="js-example-basic-single js-states form-control" id="assistantDriver" name="assistantDriver" >
				<option value="" style="display:none;">Select Assistant Driver</option>
                @foreach ($drivers as $value)
                    <option value="{{ $value->identityNumber }}"  {{ ( $value->identityNumber == old('assistantDriver',optional($trip)->assistantDriver)) ? 'selected' : '' }}>{{$value->identityNumber}}</option>
                @endforeach
			</select>
			@error('assistantDriver')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>


	<div class="form-group">
		<label for="name1">Trip Number</label>
		<input type="text" id="tripNumber" name="tripNumber" class="form-control" placeholder="Enter Trip Number" value="{{old('tripNumber', optional($trip)->tripNumber)}}" > 
		@error('tripNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 


	<div class="form-group">
		<label for="name1">Exp. Dep. Time</label>
		<input type="text" id="datepicker1" name="expectedDepartureTime" class="form-control start_time" placeholder="Enter exp. Dep. Time" value="{{old('expectedDepartureTime', optional($trip)->expectedDepartureTime)}}"  autocomplete="off"> 
		@error('expectedDepartureTime')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  



	<div class="form-group">
		<label for="name1">Exp. Arr. Time</label>
		<input type="text" id="expectedArrivalTime" name="expectedArrivalTime" class="form-control end_time" placeholder="Enter Exp. Arr. Time" value="{{old('expectedArrivalTime', optional($trip)->expectedArrivalTime)}}" autocomplete="off"> 
		@error('expectedArrivalTime')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	

	<div class="form-group">
		<label for="name1">Distance In Meters</label>
		<input type="text" id="distanceInMeters" name="distanceInMeters" class="form-control" placeholder="Enter Distance In Meters" value="{{old('distanceInMeters', optional($trip)->distanceInMeters)}}"  > 
		@error('distanceInMeters')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 


	<div class="form-group">
		<label for="name1">Number Of Passengers</label>
		<input type="text" id="numberOfPassengers" name="numberOfPassengers" class="form-control" placeholder="Enter Number Of Passengers" value="{{old('numberOfPassengers', optional($trip)->numberOfPassengers)}}" > 
		@error('numberOfPassengers')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Trip Type
			</label>
			<select class="js-example-basic-single js-states form-control" id="type" name="type" >
				<option value="" style="display:none;">Select Trip Type</option>
                @foreach ($tripTypes as $value)
                    <option value="{{ $value->code }}"  {{ ( $value->code == old('type',optional($trip)->type)) ? 'selected' : '' }}>{{$value->english_name}}</option>
                @endforeach
			</select>
			@error('type')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Wasl Activity
			</label>
			<select class="js-example-basic-single js-states form-control" id="activity" name="activity" >
				<option value="" style="display:none;">Select Wasl Activity</option>
                @foreach ($activities as $value)
                    <option value="{{ $value->code }}"  {{ ( $value->code == old('activity',optional($trip)->activity)) ? 'selected' : '' }}>{{$value->english_name}}</option>
                @endforeach
			</select>
			@error('activity')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Departure Station Code
			</label>
			<select class="js-example-basic-single js-states form-control" id="departureStationCode" name="departureStationCode" >
				<option value="" style="display:none;">Select Departure Station Code </option>
                @foreach ($stations as $value)
                    <option value="{{ $value->stationCode }}"  {{ ( $value->stationCode == old('departureStationCode',optional($trip)->departureStationCode)) ? 'selected' : '' }}>{{$value->englishName}}</option>
                @endforeach
			</select>
			@error('departureStationCode')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Arrival Station Code
			</label>
			<select class="js-example-basic-single js-states form-control" id="arrivalStationCode" name="arrivalStationCode" >
				<option value="" style="display:none;">Select Arrival Station Code </option>
                @foreach ($stations as $value)
                    <option value="{{ $value->stationCode }}"  {{ ( $value->stationCode == old('arrivalStationCode',optional($trip)->arrivalStationCode)) ? 'selected' : '' }}>{{$value->englishName}}</option>
                @endforeach
			</select>
			@error('arrivalStationCode')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
	
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">wasl Company Id</label> 
			<select class="js-example-basic-single js-states form-control" id="companyId" name="companyId" >
				<option value="" style="display:none;">Select Wasl Company Id</option>
                @foreach ($companies as $value)
                    <option value="{{ $value->companyId }}"  {{ ( $value->companyId == old('companyId',optional($trip)->companyId) )? 'selected' : '' }}>{{$value->companyName}}</option>
                @endforeach
			</select>
			@error('companyId')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
	 
	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('trips.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>