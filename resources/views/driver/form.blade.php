<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
	@error('unexpected_error')

			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	 
	<div class="form-group">
		<label for="name1">Identity Number</label>
		<input type="text" id="identityNumber" name="identityNumber" class="form-control" placeholder="Enter Identity Number" value="{{old('identityNumber', optional($driver)->identityNumber)}}" > 
		@error('identityNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
	
	<div class="form-group">
		<label for="name1">Date Of Birth Hijri</label>
		<input type="text" id="dateOfBirthHijri" name="dateOfBirthHijri" class="form-control dob" placeholder="Enter Identity Number" value="{{old('dateOfBirthHijri', optional($driver)->dateOfBirthHijri)}}" > 
		@error('dateOfBirthHijri')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<div class="form-group">
		<label for="name1">Mobile Number</label>
		<input type="text" id="mobileNumber" name="mobileNumber" class="form-control" placeholder="Enter Identity Number" value="{{old('mobileNumber', optional($driver)->mobileNumber)}}" > 
		@error('mobileNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
	 
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">wasl Company Id</label> 
			<select class="js-example-basic-single js-states form-control" id="waslCompanyId" name="waslCompanyId" >
				<option value="" style="display:none;">Select Wasl Company Id</option>
                @foreach ($companies as $value)
                    <option value="{{ $value->companyId }}"  {{ ( $value->companyId == old('waslCompanyId',optional($driver)->waslCompanyId) )? 'selected' : '' }}>{{$value->companyName}}</option>
                @endforeach
			</select>
			@error('waslCompanyId')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">
				wasl Activity
			</label>
			<select class="js-example-basic-single js-states form-control" id="waslActivity" name="waslActivity" >
				<option value="" style="display:none;">Select Wasl Activity</option>
                @foreach ($activities as $value)
                    <option value="{{ $value->code }}"  {{ ( $value->code == old('waslActivity', optional($driver)->waslActivity)) ? 'selected' : '' }}>{{$value->english_name}}</option>
                @endforeach
			</select>
			@error('waslActivity')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
	<div class="form-group">
		<label for="name1">Password</label>
		<input type="password" id="password" name="password" class="form-control" placeholder="Enter Password" value="" > 
		@error('password')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>
	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('driver.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>