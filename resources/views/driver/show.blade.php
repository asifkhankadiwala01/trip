@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Driver Detail</h4>
					</div>
				</div>
				 <div class="action-btn">
                <a href="{{ route('driver.index') }}" class="btn px-15 btn-primary" >Driver List</a>
         </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-15 py-sm-30 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-12 col-lg-10">
						<div class="mx-sm-30 mx-20 ">
					 
							<div class=" "> 

								 <div class="div mb-3">
                    <h6>IdentityNumber</h6>
                    <span>{{ $driver->identityNumber }}</span>
                </div>

								 <div class="div mb-3">
                    <h6>Date Of Birth </h6>
                    <span>{{ $driver->dateOfBirthHijri }}</span>
                </div>

								 <div class="div mb-3">
                    <h6>Mobile Number</h6>
                    <span>{{ $driver->mobileNumber }}</span>
                </div>

								 <div class="div mb-3">
                    <h6>wasl CompanyId</h6>
                    <span>{{ $driver->waslCompanyId }}</span>
                </div>

								 <div class="div mb-3">
                    <h6>wasl Activity</h6>
                    <span>{{ $driver->waslActivity }}</span>
                </div>
							</div>
							 
					
						</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
</div>
@endsection

 
