@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">List Driver</h4>
						 
					</div>
					 
				</div>
				<div style="display: flex;">
					<div class="action-btn">
						<a href="{{route('driver.create')}}" class="btn px-15 btn-primary" >
							<i class="las la-plus fs-16"></i>Add New Driver</a> 
					</div>
			    </div>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
            <div class="userDatatable global-shadow border p-30 bg-white radius-xl w-100 mb-30">
				<div class="col-md-12">
					@if ($message = Session::get('success'))
					<div class="alert alert-success mt-2 mb-2">
					  <p>{{ $message }}</p>
					</div>
					@endif
				</div>
                            <div class="table-responsive">
                                <table id="datatabled" class="table mb-0 table-borderless">
						<thead>
							<tr class="userDatatable-header">						   
								<th>
									<span class="userDatatable-title"> identityNumber</span>
								</th>
								<th>
									<span class="userDatatable-title">Date Of Birth  </span>
								</th>
								<th>
									<span class="userDatatable-title">Mobile Number</span>
								</th>
								<th>
									<span class="userDatatable-title">wasl Company Id</span>
								</th>
								<th>
									<span class="userDatatable-title">wasl Activity</span>
								</th>
								<th>
									<span class="userDatatable-title float-right">Action</span>
								</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
 
<form action="" method="POST" class="remove-record-form">
    <div class="modal fade" id="DeleteRecordmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <p></p>
            <h4>Are You Sure?</h4>
            <p>Delete This Record?</p>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary waves-effect waves-light">Yes, Delete it!</button>
            <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">No Cancel!</button>
          </div>
        </div>
      </div>
    </div>
</form>
@endsection
{{--
@section('script')
<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#datatabled').DataTable({
            // "ordering": false
            columnDefs: [
               { orderable: false, targets: [1,-1] },
            ],
            // "bStateSave": true,
            "bProcessing": true,
            "serverSide": true,
            "ajax":{
                url :$('#datatablelink').text(), // json datasource
                type: "post",  // type of method  ,GET/POST/DELETE
                "data": function (d) {
                    d._token = "{{ csrf_token() }}";
                },
                error: function(){
                    $("#datatable_grid_processing").css("display","none");
                }
            },
        });
    });
</script> -->
@endsection
 --}}

@push('css')
 <!-- dataTable CSS -->
<link href="{{asset('js/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.colVis.min.js"></script>
    <script>
         $(document).ready(function () {
            $('#datatabled').DataTable({
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'frtlp',
                language: {
                    "search": '',
                    "searchPlaceholder": "Search..."
                },
                order: [[0, "asc"]],
                serverSide: true,
                processing: true,
                 "ajax": {
                    "url": "{{url('driver/list')}}",
                    "type": "POST"
                },
                columns: [

                    {data: 'identityNumber'},
                    {data: 'dateOfBirthHijri'},
                    {data: 'mobileNumber'}, 
                    {data: 'waslCompanyId'}, 
                    {data: 'waslActivity'}, 
                    {data: 'action', orderable: false, searchable: false}
                ],
                columnDefs: [ 
                    {
                        "width": "100px",
                        "targets": 5
                    }
                ]
            });
            $('.dataTables_filter input').attr('class','form-control input-lg');
            $('.dataTables_filter input').attr('aria-describedby','search-addon');
            $('.dataTables_filter input').wrap( '<div class="input-group form-material"></div>' );
            // $('.dataTables_filter input').before('<span class="input-group-addon" id="search-addon"><i class="fa fa-search"></i></span>');
            $('.dataTables_length select').addClass('form-control input-sm');
        });
    </script>
@endpush