<!doctype html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME')}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css')  }}">
    <link rel="stylesheet" href="{{ asset('libraries/assets/vendor_assets/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/style.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}"> 
      @stack('css')
    <style type="text/css">
        
        .btn-info.btn-outline {
            color: #1e88e5;
            background-color: transparent;
        }
        .btn-circle {
            width: 25px;
            height: 25px;
            padding: 3px 0;
            border-radius: 15px;
            text-align: center;
            font-size: 12px;
            line-height: 1.428571429 !important;
            display: inline-block;
        }
        .btn-circle i {
            margin: 0;
            font-size: 11px;
        }
        .invalid-feedback {
            display: block!important;
        }
        .userDatatable table thead tr th:last-child {
            border-radius: unset !important;

        }
        .userDatatable table thead tr th:first-child {
             border-radius: unset !important;
        }
        .userDatatable-title {
            font-size: 10px;
        }
        body {
            font-size: 12px;
        }
        .sidebar {
            width: 200px;
        }
        .contents {
            padding: 74px 15px 72px 200px;
            
        }
        table.dataTable thead th, table.dataTable thead td {
             padding: 10px 20px;
                
        }
         
        #datatabled_length {
            display: none;
        }
        table.dataTable {
             margin-top: 0px;
        }
    </style>

      <script type="text/javascript">
          const APP_URL = "{{ env('APP_URL') }}";
      </script>
</head>

<body class="layout-light side-menu">
<div class="mobile-search"></div>

<div class="mobile-author-actions"></div>
<header class="header-top" style="height: 75px;">
    <nav class="navbar navbar-light">
          <div class="navbar-left">
             <!--  <a href="" class="sidebar-toggle">
                <img class="svg" src="{{ asset('img/bars.svg') }}" alt="img">
            </a> -->

            <a class="navbar-brand h4" href="/dashboard">
                {{ env('APP_NAME')}}
            </a> 
            </div>
        <!-- ends: navbar-left -->
        <!-- ends: navbar-left -->
        <div class="navbar-right">
            <ul class="navbar-right__menu">
                <li class="nav-flag-select">
                </li>
                <!-- ends: .nav-flag-select -->
                <li class="nav-author">
                    <div class="dropdown-custom" style="font-size: 16px;">
                        <a class="nav-item-toggle" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                              Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <!-- <div class="dropdown-wrapper">
                            
                            <div class="nav-author__options">
                               
                                     <a class="nav-author__signout" href="{{ route('logout') }}" 
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                         <span data-feather="log-out"></span> Sign Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                            </div>
                        </div> -->
                        <!-- ends: .dropdown-wrapper -->
                    </div>
                </li>
                <!-- ends: .nav-author -->
            </ul>
            <!-- ends: .navbar-right__menu -->
            <div class="navbar-right__mobileAction d-md-none">
                <a href="#" class="btn-author-action">
                    <span data-feather="more-vertical"></span></a>
            </div>
        </div>
        <!-- ends: .navbar-right -->
    </nav>
</header>
<main class="main-content">
    @include('layouts.sidemenu')
    <div class="contents">
        @yield('content')
    </div>
    <footer class="footer-wrapper">
        <div class="container-fluid">
        </div>
    </footer>
</main>
<div class="overlay-dark-sidebar"></div>
<div class="customizer-overlay"></div>
 @stack('js-variables')
<script src="{{ asset('libraries/assets/vendor_assets/js/jquery/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on("click",".remove-record",function() {
        event.preventDefault();
        // var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var token = $('meta[name="csrf-token"]').attr('content');
        $(".remove-record-form").attr("action",url);
        $('.remove-record-form').append('<input name="_token" type="hidden" value="'+ token +'">');
        // $('.remove-record-form').append('<input name="_method" type="hidden" value="DELETE">');
        // $('body').find('.remove-record-model').append('<input name="id" type="hidden" value="'+ id +'">');
        $('#DeleteRecordmodal').modal('show');

    });
     
</script>
 @stack('js')
 @stack('scripts')
</body>
</html>