
<aside class="sidebar">
    <div class="sidebar__menu-group">
        <ul class="sidebar_nav">
            <!-- <li class="menu-title">
                <span>Main menu</span>
            </li> -->
            <li>
                <a href="{{ route('dashboard')}}" class="">
                    <span  class="nav-icon"><i class="fas fa-home"></i></span>
                    <span class="menu-text">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ route('driver.index')}}" class="">
                    <span  class="nav-icon"><i class="fas fa-user"></i></span>
                    <span class="menu-text">Driver</span>
                </a>
            </li>

            <li>
                <a href="/vehicles" class="">
                    <span  class="nav-icon"><i class="fas fa-bus"></i></span>
                    <span class="menu-text">Vehicle</span>
                </a>
            </li>
            <li>
                <a href="/stations" class="">
                    <span  class="nav-icon"><i class="fas fa-building"></i></span>
                    <span class="menu-text">Stations</span>
                </a>
            </li>
            <li>
                <a href="/trips" class="">
                    <span  class="nav-icon"><i class="fas fa-road"></i></span>
                    <span class="menu-text">Trip</span>
                </a>
            </li>
            <li>
                <a href="/companies" class="">
                    <span  class="nav-icon"><i class="fas fa-university"></i></span>
                    <span class="menu-text">Companies</span>
                </a>
            </li>
            <li>
                <a href="/screens" class="">
                    <span  class="nav-icon"><i class="fas fa-mobile"></i></span>
                    <span class="menu-text">Screen</span>
                </a>
            </li>
         
        </ul>
    </div>
</aside>