@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="note-wrapper">
        <div class="row">
            <div class="col-lg-12">

                <div class="breadcrumb-main">
                    <h4 class="text-capitalize breadcrumb-title">Dashboard</h4>
                    <div class="breadcrumb-action justify-content-center flex-wrap">
                    </div>
                </div>

            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <!-- Card 2 End  -->
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div> 
                                <div class="overview-content">
                                    <h1>{{ $user }}</h1>
                                    <p>Drivers</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-user" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div>
                                <div class="overview-content">
                                    <h1>{{$vehicle}}</h1>
                                    <p>Vehicle</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-bus" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div>
                                <div class="overview-content">
                                    <h1>{{$station}}</h1>
                                    <p>Station</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-building" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div>
                                <div class="overview-content">
                                    <h1>{{ $trip}}</h1>
                                    <p>Trip</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-road" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div>
                                <div class="overview-content">
                                    <h1>{{ $companies}}</h1>
                                    <p>Companies</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-university" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-xxl-3 col-ssm-12 mb-30">
                        <div class="ap-po-details ap-po-details--2 p-25 radius-xl bg-white d-flex justify-content-between pb-0" style="padding-bottom:0px !important">
                            <div>
                                <div class="overview-content">
                                    <h1>{{ $screens}}</h1>
                                    <p>Screen</p>   
                                </div>
                            </div>
                            <div class="ap-po-timeChart">
                                <i class="fas fa-mobile" style="font-size:60px"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div> 
        </div>
    </div>
</div>
@endsection 