@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Add Station</h4>
					</div>
				</div>
				 <div class="action-btn">
                        <a href="{{ route('stations.index') }}" class="btn px-15 btn-primary" >Cancel</a>
                 </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-30 py-sm-50 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-12 col-lg-10">
						<div class="mx-sm-30 mx-20 ">
							<!-- Start: card -->
							<!-- <div class="card add-product p-sm-30 p-20 mb-30">
									<div class="card-body p-0">
										<div class="card-header">
											<h6 class="fw-500">Add Product</h6>
										</div> -->
										<!-- Start: card body -->
										<div class="add-product__body px-sm-40 px-20">
											<form method="POST" action="{{ route('stations.store') }}" accept-charset="UTF-8" id="create_user_form" name="create_user_form" class="form-horizontal" enctype="multipart/form-data">
													{{ csrf_field() }}
													@include ('stations.form', ['station' => null])
											</form>
										</div>
								<!-- </div>
							</div> -->
					
						</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
</div>
@endsection
