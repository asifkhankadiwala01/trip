<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
	@error('unexpected_error')

			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror

	<div class="form-group">
		<label for="name1">Station Code</label>
		<input type="text" id="stationCode" name="stationCode" class="form-control" placeholder="Enter Station Code" value="{{old('stationCode', optional($station)->stationCode)}}" > 
		@error('stationCode')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<div class="form-group">
		<label for="name1">Arabic Name</label>
		<input type="text" id="arabicName" name="arabicName" class="form-control" placeholder="Enter Arabic Name" value="{{old('arabicName', optional($station)->arabicName)}}" > 
		@error('arabicName')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
	<div class="form-group">
		<label for="name1">English Name</label>
		<input type="text" id="englishName" name="englishName" class="form-control" placeholder="Enter English Name" value="{{old('englishName', optional($station)->englishName)}}" > 
		@error('englishName')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 

	
	<div class="form-group">
		<label for="name1">Latitude</label>
		<input type="text" id="latitude" name="latitude" class="form-control" placeholder="Enter Latitude" value="{{old('latitude', optional($station)->latitude)}}" > 
		@error('latitude')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<div class="form-group">
		<label for="name1">Longitude</label>
		<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Enter Longitude" value="{{old('longitude', optional($station)->longitude)}}" > 
		@error('longitude')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 
 
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">City Code </label>
			<select class="js-example-basic-single js-states form-control" id="cityCode" name="cityCode" >
				<option value="" style="display:none;">Select City</option>
                @foreach ($cities as $value)
                    <option value="{{ $value->code }}"  {{ ( $value->code == old('cityCode', optional($station)->cityCode)) ? 'selected' : '' }}>{{$value->englishName}}</option>
                @endforeach
			</select>
			@error('cityCode')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption"> Status </label>
			<select class="js-example-basic-single js-states form-control" id="stationStatus" name="stationStatus" >
				<option value="" style="display:none;">Select Status</option>
                @foreach (['ACTIVE','INACTIVE'] as $value)
                    <option value="{{ $value }}"  {{ ( $value == old('stationStatus',optional($station)->stationStatus)) ? 'selected' : '' }}>{{$value}}</option>
                @endforeach
			</select>
			@error('stationStatus')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>



	<div class="form-group">
		<label for="name1">Email</label>
		<input type="text" id="email" name="email" class="form-control" placeholder="Enter Email" value="{{old('email', optional($station)->email)}}" > 
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 


	<div class="form-group">
		<label for="name1">Phone</label>
		<input type="text" id="phone" name="phone" class="form-control" placeholder="Enter Phone" value="{{old('phone', optional($station)->phone)}}" > 
		@error('phone')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 
	<div class="form-group">
		<label for="name1">Phone extension</label>
		<input type="text" id="phoneExtension" name="phoneExtension" class="form-control" placeholder="Enter Phone extension" value="{{old('phoneExtension', optional($station)->phoneExtension)}}" > 
		@error('phoneExtension')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 

	<div class="form-group">
		<label for="name1">Manager phone</label>
		<input type="text" id="managerPhone" name="managerPhone" class="form-control" placeholder="Enter Manager phone" value="{{old('managerPhone', optional($station)->managerPhone)}}" > 
		@error('managerPhone')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
  
	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('stations.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>