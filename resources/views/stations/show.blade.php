@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Driver Detail</h4>
					</div>
				</div>
				 <div class="action-btn">
                <a href="{{ route('driver.index') }}" class="btn px-15 btn-primary" >Driver List</a>
         </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-15 py-sm-30 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-12 col-lg-10">
						<div class="mx-sm-30 mx-20 ">
					 
							<div class=" "> 
								
								 <div class="div mb-3">
				                    <h6>Station Code</h6>
				                    <span>{{ $station->stationCode }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Arabic Name</h6>
				                    <span>{{ $station->arabicName }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>English Name</h6>
				                    <span>{{ $station->englishName }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Latitude</h6>
				                    <span>{{ $station->latitude }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Longitude</h6>
				                    <span>{{ $station->longitude }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>City Code</h6>
				                    <span>{{ $station->cityCode }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Station Status</h6>
				                    <span>{{ $station->stationStatus }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Email</h6>
				                    <span>{{ $station->email }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Phone</h6>
				                    <span>{{ $station->phone }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Phone Extension</h6>
				                    <span>{{ $station->phoneExtension }}</span>
				                </div> 
						 
								 <div class="div mb-3">
				                    <h6>Manager Phone</h6>
				                    <span>{{ $station->managerPhone }}</span>
				                </div> 
						 
							</div>
							 
					
						</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
</div>
@endsection

 
