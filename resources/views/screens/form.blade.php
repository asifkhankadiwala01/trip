<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
	@error('unexpected_error')
		<span class="invalid-feedback" role="alert">
			<strong>{{ $message }}</strong>
		</span>
	@enderror

	<div class="form-group">
		<label for="name1">Screen Id</label>
		<input type="text" id="screen_id" name="screen_id" class="form-control" placeholder="Enter Screen Id" value="{{old('screen_id', optional($screens)->screen_id)}}" > 
		@error('screen_id')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">Company Id </label> 
			<select class="js-example-basic-single js-states form-control" id="company_id" name="company_id" >
				<option value="" style="display:none;">Select Company Id</option>
                @foreach ($companies as $value)
                    <option value="{{ $value->companyId }}"  {{ ( $value->companyId == old('company_id',optional($screens)->company_id) )? 'selected' : '' }}>{{$value->companyName}}</option>
                @endforeach
			</select>
			@error('company_id')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	<div class="form-group">
		<label for="name1">Rest User Id</label>
		<input type="text" id="rest_user_id" name="rest_user_id" class="form-control" placeholder="Enter Rest User Id" value="{{old('rest_user_id', optional($screens)->rest_user_id)}}" > 
		@error('rest_user_id')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 

	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('screens.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>