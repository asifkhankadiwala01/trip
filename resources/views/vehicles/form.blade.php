<div class="form-basic">
    <div class="col-md-12" id="message">
        @if ($message = Session::get('error'))
        <div class="alert alert-danger">
          <p>{{ $message }}</p>
        </div>
        @endif
    </div>  
		@error('unexpected_error')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	 
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">
				wasl Driver Id
			</label>
			<select class="js-example-basic-single js-states form-control" id="waslDriverId" name="waslDriverId" >
				<option value="" style="display:none;">Select Wasl Driver Id</option>
                @foreach ($drivers as $driver)
                    <option value="{{ $driver->identityNumber }}"  {{ ( $driver->identityNumber == old('waslDriverId', optional($vehicle)->waslDriverId)) ? 'selected' : '' }}>{{$driver->identityNumber}}</option>
                @endforeach
			</select>
			@error('waslDriverId')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">wasl Company Id</label> 
			<select class="js-example-basic-single js-states form-control" id="waslCompanyId" name="waslCompanyId" >
				<option value="" style="display:none;">Select Wasl Company Id</option>
                @foreach ($companies as $value)
                    <option value="{{ $value->companyId }}"  {{ ( $value->companyId == old('waslCompanyId',optional($vehicle)->waslCompanyId) )? 'selected' : '' }}>{{$value->companyName}}</option>
                @endforeach
			</select>
			@error('waslCompanyId')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>

	<div class="form-group">
		<label for="name1">gps Gate User Id</label>
		<input type="text" id="gpsGateUserId" name="gpsGateUserId" class="form-control" placeholder="Enter gps Gate User Id" value="{{old('gpsGateUserId', optional($vehicle)->gpsGateUserId)}}" > 
		@error('gpsGateUserId')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 


	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">
				gps Gate Server Id
			</label>
			<select class="js-example-basic-single js-states form-control" id="gpsGateServerId" name="gpsGateServerId" >
				<option value="" style="display:none;">Select gps Gate Server Id</option>
                <option value="1"  {{ ( 1 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>1</option>
                <option value="2"  {{ ( 2 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>2</option>
                <option value="3"  {{ ( 3 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>3</option>
                <option value="4"  {{ ( 4 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>4</option>
                <option value="5"  {{ ( 5 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>5</option>
                <option value="6"  {{ ( 6 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>6</option>
                <option value="7"  {{ ( 7 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>7</option>
                <option value="8"  {{ ( 8 == old('gpsGateServerId', optional($vehicle)->gpsGateServerId)) ? 'selected' : '' }}>8</option>
			</select>
			@error('gpsGateServerId')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
 

	<div class="form-group">
		<label for="name1">Sequence Number</label>
		<input type="text" id="sequenceNumber" name="sequenceNumber" class="form-control" placeholder="Enter sequence Number" value="{{old('sequenceNumber', optional($vehicle)->sequenceNumber)}}" > 
		@error('sequenceNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div> 
 
 

	<div class="form-group">
		<label for="name1">Plate Type</label>
		<input type="text" id="plateType" name="plateType" class="form-control" placeholder="Enter plate Type" value="{{old('plateType', optional($vehicle)->plateType)}}" > 
		@error('plateType')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 
 

	<div class="form-group">
		<label for="name1">Plate Number</label>
		<input type="text" id="plateNumber" name="plateNumber" class="form-control" placeholder="Enter plate Number" value="{{old('plateNumber', optional($vehicle)->plateNumber)}}" > 
		@error('plateNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 
 


	<div class="form-group">
		<label for="name1">Plate Right Letter</label>
		<input type="text" id="plateRightLetter" name="plateRightLetter" class="form-control" placeholder="Enter plate Right Letter" value="{{old('plateRightLetter', optional($vehicle)->plateRightLetter)}}" > 
		@error('plateRightLetter')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 


	<div class="form-group">
		<label for="name1">Plate Middle Letter</label>
		<input type="text" id="plateMiddleLetter" name="plateMiddleLetter" class="form-control" placeholder="Enter plate Middle Letter" value="{{old('plateMiddleLetter', optional($vehicle)->plateMiddleLetter)}}" > 
		@error('plateMiddleLetter')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 

	<div class="form-group">
		<label for="name1">Plate Left Letter</label>
		<input type="text" id="plateLeftLetter" name="plateLeftLetter" class="form-control" placeholder="Enter plate Left Letter" value="{{old('plateLeftLetter', optional($vehicle)->plateLeftLetter)}}" > 
		@error('plateLeftLetter')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 
 
	<div class="form-group">
		<label for="name1">imei Number</label>
		<input type="text" id="imeiNumber" name="imeiNumber" class="form-control" placeholder="Enter imei Number" value="{{old('imeiNumber', optional($vehicle)->imeiNumber)}}" > 
		@error('imeiNumber')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>  
 
	<div class="form-group">
		<div class="countryOption">
			<label for="countryOption">
				wasl Activity
			</label>
			<select class="js-example-basic-single js-states form-control" id="waslActivity" name="waslActivity" >
				<option value="" style="display:none;">Select Wasl Activity</option>
                @foreach ($activities as $value)
                    <option value="{{ $value->code }}"  {{ ( $value->code ==  old('waslActivity', optional($vehicle)->waslActivity) ) ? 'selected' : '' }}>{{$value->english_name}}</option>
                @endforeach
			</select>
			@error('waslActivity')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
 

	<!-- Start: button group -->
	<div class="button-group add-product-btn d-flex justify-content-end mt-40">
		<button class="btn btn-light btn-default btn-squared fw-400 text-capitalize"> <a  href="{{ route('vehicles.index') }}"> cancel</a></button>
		<button class="btn btn-primary btn-default btn-squared text-capitalize">Save</button>
	</div>
	<!-- End: button group -->
</div>