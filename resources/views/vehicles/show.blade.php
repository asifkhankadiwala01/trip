@extends('layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="breadcrumb-main user-member justify-content-sm-between ">
				<div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
					<div class="d-flex align-items-center user-member__title justify-content-center mr-sm-25">
						<h4 class="text-capitalize fw-500 breadcrumb-title">Driver Detail</h4>
					</div>
				</div>
				 <div class="action-btn">
                <a href="{{ route('driver.index') }}" class="btn px-15 btn-primary" >Driver List</a>
         </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<!-- Start: product page -->
			<div class="global-shadow border px-sm-15 py-sm-30 px-0 py-20 bg-white radius-xl w-100 mb-40">
				<div class="row  ">
					<div class="col-xl-12 col-lg-10">
						<div class="mx-sm-30 mx-20 ">
					 
							<div class=" "> 

								 <div class="div mb-3">
				                    <h6>Wasl Driver Id</h6>
				                    <span>{{ $vehicle->waslDriverId }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Wasl Company Id</h6>
				                    <span>{{ $vehicle->waslCompanyId }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Gps Gate User Id</h6>
				                    <span>{{ $vehicle->gpsGateUserId }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Gps Gate Server Id</h6>
				                    <span>{{ $vehicle->gpsGateServerId }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Sequence Number</h6>
				                    <span>{{ $vehicle->sequenceNumber }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Plate Type</h6>
				                    <span>{{ $vehicle->plateType }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Plate Number</h6>
				                    <span>{{ $vehicle->plateNumber }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Plate Right Letter</h6>
				                    <span>{{ $vehicle->plateRightLetter }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Plate Middle Letter</h6>
				                    <span>{{ $vehicle->plateMiddleLetter }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Plate Left Letter</h6>
				                    <span>{{ $vehicle->plateLeftLetter }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Imei Number</h6>
				                    <span>{{ $vehicle->imeiNumber }}</span>
				                </div>

								 <div class="div mb-3">
				                    <h6>Wasl Activity</h6>
				                    <span>{{ $vehicle->waslActivity }}</span>
				                </div>
						 
							</div>
							 
					
						</div>
					</div>
				</div>
			</div>
			<!-- End: Product page -->
		</div>
		<!-- ends: col-lg-12 -->
	</div>
</div>
@endsection

 
