<div style="width: 115px;">
<a href="{{url($prefix.'/show/'.$table->id)}}" class="btn btn-info btn-outline btn-circle m-r-5" data-toggle="tooltip" title="View"><i class="fas fa-eye"></i></a>
 
<a href="{{url($prefix.'/edit/'.$table->id)}}" class="btn btn-info btn-outline btn-circle m-r-5" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a>
  <!-- href="{{url($prefix.'/'.$table->id.'/delete')}}" -->
<a data-url="{{ url($prefix.'/delete', $table->id)}}" class="btn btn-info btn-outline btn-circle m-r-5 remove-record" data-toggle="tooltip" title="Delete">
    <i class="fas fa-trash"></i>
</a>  
</div>
<script>
    // $(document).find('[data-toggle="tooltip"]').tooltip()
</script>

 