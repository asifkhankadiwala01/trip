<?php
return [
	'tripTypes'=>[
		(object)[
			'code'=> 'INSIDE_CITY',
			'english_name'=> 'Inside a city',
			'arabic_name'=> 'المدينة داخل',

		],
		(object)[
			'code'=> 'BETWEEN_CITIES',
			'english_name'=> 'Between cites',
			'arabic_name'=> 'المدن ب',

		],
	],
	'activities'=>[
		(object)[
			'code'=> 'EDUCATIONAL_TRANSPORT',
			'english_name'=> 'Educational Transport',
			'arabic_name'=> 'التعليمي النقل'

		],
		(object)[
			'code'=> 'BUS_RENTAL',
			'english_name'=> 'Bus Rental',
			'arabic_name'=> 'الحافالت تأجير'

		],
		(object)[
			'code'=> 'SPECIALITY_TRANSPORT',
			'english_name'=> 'Specialty Transport',
			'arabic_name'=> 'المتخصص النقل'

		],
	],
];


 