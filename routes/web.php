<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\StationController;
use App\Http\Controllers\TripController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ScreenController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/import', [App\Http\Controllers\ImportController::class, 'uploadContent']);
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

 


Route::group(['prefix' => 'driver','as'=>'driver.'], function () {
    Route::get('/', [DriverController::class, 'index'])->name('index');
    Route::post('/list', [DriverController::class, 'list'])->name('list');
    Route::get('/create', [DriverController::class, 'create'])->name('create');
    Route::get('/show/{id}', [DriverController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [DriverController::class, 'edit'])->name('edit');

    Route::post('/store', [DriverController::class, 'store'])->name('store');
    Route::post('/update/{id}', [DriverController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [DriverController::class, 'destroy'])->name('destroy');
    
});

Route::group(['prefix' => 'vehicles','as'=>'vehicles.'], function () {
    Route::get('/', [VehicleController::class, 'index'])->name('index');
    Route::post('/list', [VehicleController::class, 'list'])->name('list');
    Route::get('/create', [VehicleController::class, 'create'])->name('create');
    Route::get('/show/{id}', [VehicleController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [VehicleController::class, 'edit'])->name('edit');

    Route::post('/store', [VehicleController::class, 'store'])->name('store');
    Route::post('/update/{id}', [VehicleController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [VehicleController::class, 'destroy'])->name('destroy');
});

Route::group(['prefix' => 'stations','as'=>'stations.'], function () {
    Route::get('/', [StationController::class, 'index'])->name('index');
    Route::post('/list', [StationController::class, 'list'])->name('list');
    Route::get('/create', [StationController::class, 'create'])->name('create');
    Route::get('/show/{id}', [StationController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [StationController::class, 'edit'])->name('edit');

    Route::post('/store', [StationController::class, 'store'])->name('store');
    Route::post('/update/{id}', [StationController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [StationController::class, 'destroy'])->name('destroy');
});

Route::group(['prefix' => 'trips','as'=>'trips.'], function () {
    Route::get('/', [TripController::class, 'index'])->name('index');
    Route::post('/list', [TripController::class, 'list'])->name('list');
    Route::get('/create', [TripController::class, 'create'])->name('create');
    Route::get('/show/{id}', [TripController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [TripController::class, 'edit'])->name('edit');

    Route::post('/store', [TripController::class, 'store'])->name('store');
    Route::post('/update/{id}', [TripController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [TripController::class, 'destroy'])->name('destroy');
});

Route::group(['prefix' => 'companies','as'=>'companies.'], function () {
    Route::get('/', [CompanyController::class, 'index'])->name('index');
    Route::post('/list', [CompanyController::class, 'list'])->name('list');
    Route::get('/create', [CompanyController::class, 'create'])->name('create');
    Route::get('/show/{id}', [CompanyController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [CompanyController::class, 'edit'])->name('edit');

    Route::post('/store', [CompanyController::class, 'store'])->name('store');
    Route::post('/update/{id}', [CompanyController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [CompanyController::class, 'destroy'])->name('destroy');
});

Route::group(['prefix' => 'screens','as'=>'screens.'], function () {
    Route::get('/', [ScreenController::class, 'index'])->name('index');
    Route::post('/list', [ScreenController::class, 'list'])->name('list');
    Route::get('/create', [ScreenController::class, 'create'])->name('create');
    Route::get('/show/{id}', [ScreenController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [ScreenController::class, 'edit'])->name('edit');

    Route::post('/store', [ScreenController::class, 'store'])->name('store');
    Route::post('/update/{id}', [ScreenController::class, 'update'])->name('update');
    Route::post('/delete/{id}', [ScreenController::class, 'destroy'])->name('destroy');
});