<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
 use App\Http\Controllers\Api\AuthController;
 use App\Http\Controllers\Api\TripController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([ 'middleware' => 'api' ], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    
    Route::group(['middleware' => 'jwt.auth'], function () {

        Route::get('get_trips', [TripController::class, 'getTrips']);
        Route::post('change_trips_status', [TripController::class, 'ChangeTripsStatus']);
    


    });
  

});
