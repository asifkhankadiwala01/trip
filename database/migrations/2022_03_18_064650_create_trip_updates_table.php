<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_updates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("trip_id")->nullable();
            $table->string("driverId")->nullable();
            $table->string("numberOfPassengersIn")->nullable();
            $table->string("numberOfPassengersOut")->nullable();
            $table->string("tripStatus")->nullable();
            $table->string("currentStationCode")->nullable();
            $table->string("tripUpdateDateTime")->nullable();
            $table->string("activity")->nullable();
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_updates');
    }
}
