<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->string("vehicle")->nullable();
            $table->string("primaryDriver")->nullable();
            $table->string("assistantDriver")->nullable();
            $table->string("tripNumber")->nullable();
            $table->string("expectedDepartureTime")->nullable();
            $table->string("expectedArrivalTime")->nullable();
            $table->string("distanceInMeters")->nullable();
            $table->string("numberOfPassengers")->nullable();
            $table->string("type")->nullable();
            $table->string("activity")->nullable();
            $table->string("departureStationCode")->nullable();
            $table->string("arrivalStationCode")->nullable();
            $table->string("companyId")->nullable();
			$table->string("trip_status")->default('PLANNED')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
