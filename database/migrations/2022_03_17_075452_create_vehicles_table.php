<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('waslDriverId')->nullable();
            $table->string('waslCompanyId')->nullable();
            $table->string('gpsGateUserId')->nullable();
            $table->string('gpsGateServerId')->nullable();
            $table->string('sequenceNumber')->nullable();
            $table->string('plateType')->nullable();
            $table->string('plateNumber')->nullable();
            $table->string('plateRightLetter')->nullable();
            $table->string('plateMiddleLetter')->nullable();
            $table->string('plateLeftLetter')->nullable();
            $table->string('imeiNumber')->nullable();
            $table->string('waslActivity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
