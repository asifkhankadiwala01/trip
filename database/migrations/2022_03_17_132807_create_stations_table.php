<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationsTable extends Migration
{













    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->id();
            $table->string("stationCode")->nullable();
            $table->string("arabicName")->nullable();
            $table->string("englishName")->nullable();
            $table->string("latitude")->nullable();
            $table->string("longitude")->nullable();
            $table->string("cityCode")->nullable();
            $table->string("stationStatus")->nullable();
            $table->string("email")->nullable();
            $table->string("phone")->nullable();
            $table->string("phoneExtension")->nullable();
            $table->string("managerPhone")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
